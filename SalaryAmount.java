import java.util.*;

public class SalaryAmount {
    public static void main(String[] args) {
        int[] workingHoursPerDay = new int[10];
        Scanner sc = new Scanner(System.in);
        for (int i=0; i<7; i++){
            int input = sc.nextInt();
            if(input<0 || input>24){
                System.out.println("Invalid Input!!");
                i--;
                continue;
            }
            workingHoursPerDay[i] = input;
        }

        int totalWorkingHours = 0;
        double totalSalaryAmount = 0.0;

        for(int hours = 0; hours < 7; hours++){
            totalWorkingHours += workingHoursPerDay[hours];
            int workAmount = (workingHoursPerDay[hours]<=8) ? workingHoursPerDay[hours]*100 : workingHoursPerDay[hours]*100+(workingHoursPerDay[hours]-8)*15;
//            System.out.println("Amount: "+workAmount);
            if(workAmount>0) {
                if (hours == 0)
                    totalSalaryAmount += (0.5) * workAmount;
                if (hours == 6)
                    totalSalaryAmount += (0.25) * workAmount;
            }
            totalSalaryAmount += workAmount;
        }

        if(totalWorkingHours > 40)
            totalSalaryAmount += (totalWorkingHours - 40)*25;

        System.out.println(totalSalaryAmount);
    }
}
